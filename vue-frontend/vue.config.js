module.exports = {
    devServer: {
        proxy: {
            '^/api': {
                target: 'http://localhost:8090/api',
                changeOrigin: true,
                ws: true,
                logLevel: 'debug',
                pathRewrite: { '^/api': '/'}
            }
        }
    }    
}