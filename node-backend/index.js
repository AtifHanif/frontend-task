const express = require('express');
const path = require('path');
var bodyParser = require('body-parser')

//const exphbs = require('express-handlebars');
// const logger = require('./middleware/logger');
// const members = require('./Members');
const app = express();

// Auth API Routes
app.use('/api/auth', require('./routes/api/auth'));
app.use('/api/CRUD', require('./routes/api/CRUD'));




const PORT = process.env.PORT || 8090;
app.listen(PORT, () => console.log(`Server started on port ${PORT}`));
