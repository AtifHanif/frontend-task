const roles = [
    {
      id: 1,
      name: 'student',
      permissions: [
          {name: 'Register Exam', componentId: 'register-exam'},
          {name: 'View Scores', componentId: 'view-scores'}
        ]
    },
    {
      id: 2,
      name: 'professor',
      permissions: [
        {name: 'Upload Scores', componentId: 'upload-scores'},
        {name: 'View Scores', componentId: 'view-scores'}
      ]
    },
    {
      id: 3,
      name: 'office',
      permissions: [
        {name: 'Create Exam', componentId: 'create-exam'},
        {name: 'Assign Exam', componentId: 'assign-exam'},
        {name: 'Delete Exam', componentId: 'delete-exam'}
      ]
    }
  ];
  
  module.exports = roles;