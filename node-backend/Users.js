const users = [
    {
      id: 1,
      name: 'John Doe',
      email: 'john@gmail.com',
      role: 'student',
      status: 'active'
    },
    {
      id: 2,
      name: 'Bob Williams',
      email: 'bob@gmail.com',
      role: 'professor',
      status: 'inactive'
    },
    {
      id: 3,
      name: 'Shannon Jackson',
      email: 'email@gmail.com',
      role: 'office',
      status: 'active'
    }
  ];
  
  module.exports = users;
