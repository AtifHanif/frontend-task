const express = require('express');
const router = express.Router();
const bodyParser = require('body-parser');
const m_auth = require('../../middleware/authentication');
const fs = require('fs');


router.use(m_auth())
router.use(bodyParser.json())
router.post('/exam',  (req, res) => {
    console.log('Inside Exam')
    const e_obj = req.body
    const path = './data/exam.txt'
    try {
        fs.appendFile(path, JSON.stringify(e_obj)+ ',', function (err) {
            if (err) throw err;
            console.log('Updated!');
            console.log(e_obj)
          });
      } catch(err) {
        console.error(err)
      }

    res.status(200).json({ msg: 'Request Successful'})
});

module.exports = router;