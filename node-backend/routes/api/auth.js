require('dotenv').config()

const express = require('express');
const router = express.Router();
const users = require('../../Users');
const roles = require('../../Roles');
const jwt = require('jsonwebtoken');
const bodyParser = require('body-parser');
// const { response } = require('express');


router.post('/', bodyParser.json(), (req, res) => {
    const email = req.body.email;
    const pass = req.body.pass;
    if (!validateUser(email, pass)) {
        return res.status(401).json({ msg: 'Invalid Credentials'})
    }
    const accesstoken = jwt.sign(email, process.env.ACCESS_TOKEN_SECRET)
    res.status(200).json({ accessToken: accesstoken  })
});

router.post('/validaterequest', authenticateToken, (req, res) => {
  res.sendStatus(200)
});


router.get('/menuoptions', authenticateToken, (req, res) => {
  console.log('email: ' + req.user)
  const role = users.filter( users => users.email == req.user).pop().role;
  const options = roles.filter( roles => roles.name == role ).pop().permissions
  // const menuOptions  req.user
  res.status(200).json({ menuOptions: options  })
  console.log('after validateRequest')
});

// Authentication Middleware
function authenticateToken(req, res, next) {
    const authHeader = req.headers['authorization']
    const token = authHeader && authHeader.split(' ')[1]
    if (token == null) return res.sendStatus(401)
    jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (err, user) => {
      console.log(err)
      if (err) return res.sendStatus(401)
      req.user = user
      next()
    })
  }

function validateUser(_email, _pass) {
    return true;
}
module.exports = router;